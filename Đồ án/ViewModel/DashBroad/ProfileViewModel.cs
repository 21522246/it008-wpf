﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using Đồ_án.Command;
using Đồ_án.View;
using Đồ_án.Database;
using Đồ_án.ViewModel.Login.Service;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows;
using Đồ_án.ViewModel.Login;

namespace Đồ_án.ViewModel
{
    public class ProfileViewModel : BaseViewModel
    {
        private string findID;

        public string FindID
        {
            get { return findID; }
            set { findID = value; OnPropertyChanged("FindID"); }
        }

        
        public Visibility IsAdmin2 { get; set; }

        private User student;
        public User Student
        {
            get { return student; }
            set { student = value; OnPropertyChanged("Student"); }
        }

        

        private Relative _father;
        public Relative Father
        {
            get { return _father; }
            set
            {
                _father = value;
                OnPropertyChanged();
            }
        }

        private Relative _mother;
        public Relative Mother
        {
            get { return _mother; }
            set
            {
                _mother = value;
                OnPropertyChanged();
            }
        }





        public ICommand FindCommand { get; set; }
        public ICommand ProfileCommand { get; set; }

        private Profile profile { get; set; }
        public ProfileViewModel()
        {
            ProfileCommand = new RelayCommand<object>((p) => true, p => OnExcute());
            FindCommand = new RelayCommand<object>((p) => true, Find);
            string Role = LoginServices.CurrentUser.UserRole.Role;
            if (string.Compare(Role, "SV") == 0)
            {

                
                IsAdmin2 = Visibility.Visible;
                FindID = LoginServices.CurrentUser.IdUser;
                Find(Role);

            }
            else 
            {              
                IsAdmin2 = Visibility.Collapsed;
                FindID = LoginServices.CurrentUser.IdUser;
                Find(Role);
            }
        }

        private async void Find(object obj)
        {
            User temp = UserServices.Instance.GetUserById(FindID);
            if (temp == null)
            {
                MessageBox.Show("Can't found student", "Students management", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            student = temp;
            Father = await UserServices.Instance.GetFatherbyId(FindID);
            Mother = await UserServices.Instance.GetMotherbyId(FindID);

            OnPropertyChanged(nameof(Student));
        }

        public void OnExcute()
        {
            profile = new Profile();
            ((MainWindow)System.Windows.Application.Current.MainWindow).MainFrame.Content = profile;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnProfile.Background = Brushes.CornflowerBlue;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnInsert.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnSubject.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnHome.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnSetting.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnTeacher.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnClasses.Background = Brushes.DarkSlateGray;
            ((MainWindow)System.Windows.Application.Current.MainWindow).btnSchedule.Background = Brushes.DarkSlateGray;
        }
    }
}
